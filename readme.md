# My overlayroot-chroot

Modified overlayroot-chroot to return the exit code of the program run in chroot.
Also used full path names for programs used internally by the script.
Published the changes publicly to comply with the licensing requirements.
